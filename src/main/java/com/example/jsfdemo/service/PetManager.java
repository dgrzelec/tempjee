package com.example.jsfdemo.service;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import com.example.jsfdemo.domain.Pet;

@ApplicationScoped
public class PetManager {
	private List<Pet> db = new ArrayList<Pet>();

	public void addPet(Pet pet) {
		Pet newPet = new Pet();

		newPet.setName(pet.getName());
		newPet.setGender(pet.getGender());
		newPet.setBreed(pet.getBreed());
		newPet.setColour(pet.getColour());
		newPet.setNumber(pet.getNumber());
		newPet.setDateOfBirth(pet.getDateOfBirth());
		newPet.setWeight(pet.getWeight());
		newPet.setVaccination(pet.isVaccination());

		db.add(newPet);
	}

	// Removes the pet with given Number
	public void deletePet(Pet pet) {
		Pet petToRemove = null;
		for (Pet p : db) {
			if (pet.getNumber().equals(p.getNumber())) {
				petToRemove = p;
				break;
			}
		}
		if (petToRemove != null)
			db.remove(petToRemove);
	}

	public List<Pet> getAllPets() {
		return db;
	}
}
